while True:
  try:
    def contaMadeira():
      for i in sorted(arvores.keys()):
        percentual = (arvores[i]/tamanho)*100
        print(i,round(percentual, 4))
      print("")

    n = input()
    start = input()
    arvores = {}
    tamanho = 0
    while True:
      aux = input()
      if aux != '':
        if aux in arvores:
          arvores[aux] += 1
        else:
          arvores[aux] = 1
        tamanho += 1
      else:
        contaMadeira()
        tamanho = 0
        arvores = {}
  except EOFError:
    contaMadeira()
    break
