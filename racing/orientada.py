class Carro:
    def __init__(self, name):
        self.name = name
        self.l_init = ['']
        self.r_final = ['']
    
    def setLinit(self, l_init):
        self.l_init = l_init

    def setRfinal(self, r_final):
        self.r_final = r_final

    def getLinit(self):
        return self.l_init

    def getRfinal(self):
        return self.r_final

while True:
  try:
    number = input()
    a_init = input()
    a_final = input()
    a_init = a_init.split(" ")
    a_final = a_final.split(" ")
    passing = 0
    car = []

    for i in range(0, len(a_final)):
        car = Carro(a_final[i])
        for j in range(0, len(a_init)):
            if a_init[j] == a_final[i]:
                esq = []
                for n in range(0, j):
                    esq.append(a_init[n])
                car.setLinit(esq)
                dir = []
                for m in reversed(range(i+1, len(a_final))):
                    dir.append(a_final[m])
                car.setRfinal(dir)
        l_init = car.getLinit()
        r_final = car.getRfinal()
        for k in r_final:
            if k in l_init:
                passing += 1        
    print (passing)
  except EOFError:
    break
